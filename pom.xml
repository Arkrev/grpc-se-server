<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns="http://maven.apache.org/POM/4.0.0"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>org.springframework.samples</groupId>
	<artifactId>grpc-se-server</artifactId>
	<version>1.0.0-SNAPSHOT</version>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.1.6.RELEASE</version>
	</parent>
	<name>gRPC-SE-server</name>

	<properties>

		<!-- Generic properties -->
		<java.version>1.8</java.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

		<!-- Web dependencies -->
		<webjars-bootstrap.version>3.3.6</webjars-bootstrap.version>
		<tablesaw.version>0.38.2</tablesaw.version>
		<grpc.version>1.37.0</grpc.version>
		<jacoco.version>0.8.2</jacoco.version>

	</properties>

	<dependencies>
		<!-- Spring and Spring Boot dependencies -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-cache</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-thymeleaf</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- caching -->
		<dependency>
			<groupId>javax.cache</groupId>
			<artifactId>cache-api</artifactId>
		</dependency>
		<dependency>
			<groupId>org.ehcache</groupId>
			<artifactId>ehcache</artifactId>
		</dependency>

		<!-- webjars -->
		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>webjars-locator-core</artifactId>
		</dependency>

		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>bootstrap</artifactId>
			<version>${webjars-bootstrap.version}</version>
		</dependency>
		<!-- end of webjars -->

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<optional>true</optional>
		</dependency>


		<!-- GRPC -->
		<dependency>
			<groupId>com.google.protobuf</groupId>
			<artifactId>protobuf-java</artifactId>
			<version>3.16.0</version>
		</dependency>


		<dependency>
			<groupId>io.grpc</groupId>
			<artifactId>grpc-protobuf</artifactId>
			<version>${grpc.version}</version>
		</dependency>
		<dependency>
			<groupId>io.grpc</groupId>
			<artifactId>grpc-stub</artifactId>
			<version>${grpc.version}</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/io.github.lognet/grpc-spring-boot-starter -->
		<dependency>
			<groupId>io.github.lognet</groupId>
			<artifactId>grpc-spring-boot-starter</artifactId>
			<version>4.5.0</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/org.projectlombok/lombok -->
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<scope>provided</scope>
		</dependency>

		<!-- https://mvnrepository.com/artifact/tech.tablesaw/tablesaw-core -->
		<dependency>
			<groupId>tech.tablesaw</groupId>
			<artifactId>tablesaw-core</artifactId>
			<version>${tablesaw.version}</version>
		</dependency>

		<dependency>
			<groupId>tech.tablesaw</groupId>
			<artifactId>tablesaw-excel</artifactId>
			<version>${tablesaw.version}</version>
		</dependency>

		<dependency>
			<groupId>tech.tablesaw</groupId>
			<artifactId>tablesaw-jsplot</artifactId>
			<version>${tablesaw.version}</version>
		</dependency>

		<!-- https://mvnrepository.com/artifact/com.google.guava/guava -->
		<dependency>
			<groupId>com.google.guava</groupId>
			<artifactId>guava</artifactId>
			<version>30.1.1-jre</version>
		</dependency>



	</dependencies>

	<build>
		<defaultGoal>clean generate-sources compile install</defaultGoal>

		<plugins>
			<!-- compile proto file into java files. -->
			<plugin>
				<groupId>com.github.os72</groupId>
				<artifactId>protoc-jar-maven-plugin</artifactId>
				<version>3.6.0.1</version>
				<executions>
					<execution>
						<phase>generate-sources</phase>
						<goals>
							<goal>run</goal>
						</goals>
						<configuration>
							<includeMavenTypes>direct</includeMavenTypes>
							<protocCommand>protoc</protocCommand>
							<inputDirectories>
								<include>src/main/resources</include>
							</inputDirectories>

							<outputTargets>
								<outputTarget>
									<type>java</type>
									<outputDirectory>src/main/java</outputDirectory>
								</outputTarget>
								<outputTarget>
									<type>grpc-java</type>
									<pluginArtifact>io.grpc:protoc-gen-grpc-java:1.15.0</pluginArtifact>
									<outputDirectory>src/main/java</outputDirectory>
								</outputTarget>
							</outputTargets>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<executions>
					<execution>
						<!-- Spring Boot Actuator displays build-related information if a META-INF/build-info.properties 
							file is present -->
						<goals>
							<goal>build-info</goal>
						</goals>
						<configuration>
							<additionalProperties>
								<encoding.source>${project.build.sourceEncoding}</encoding.source>
								<encoding.reporting>${project.reporting.outputEncoding}</encoding.reporting>
								<java.source>${maven.compiler.source}</java.source>
								<java.target>${maven.compiler.target}</java.target>
							</additionalProperties>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>${jacoco.version}</version>
				<executions>
					<execution>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>report</id>
						<phase>prepare-package</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- Spring Boot Actuator displays build-related information if a git.properties 
				file is present at the classpath -->
			<plugin>
				<groupId>pl.project13.maven</groupId>
				<artifactId>git-commit-id-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>revision</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<verbose>true</verbose>
					<dateFormat>yyyy-MM-dd'T'HH:mm:ssZ</dateFormat>
					<generateGitPropertiesFile>true</generateGitPropertiesFile>
					<generateGitPropertiesFilename>${project.build.outputDirectory}/git.properties
					</generateGitPropertiesFilename>
					<failOnNoGitDirectory>false</failOnNoGitDirectory>
				</configuration>
			</plugin>
		</plugins>
	</build>

	<!-- Apache 2 license -->
	<licenses>
		<license>
			<name>Apache License, Version 2.0</name>
			<url>https://www.apache.org/licenses/LICENSE-2.0</url>
		</license>
	</licenses>

	<repositories>
		<repository>
			<id>spring-snapshots</id>
			<name>Spring Snapshots</name>
			<url>https://repo.spring.io/snapshot</url>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>spring-milestones</id>
			<name>Spring Milestones</name>
			<url>https://repo.spring.io/milestone</url>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</repository>
	</repositories>

	<pluginRepositories>
		<pluginRepository>
			<id>spring-snapshots</id>
			<name>Spring Snapshots</name>
			<url>https://repo.spring.io/snapshot</url>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</pluginRepository>
		<pluginRepository>
			<id>spring-milestones</id>
			<name>Spring Milestones</name>
			<url>https://repo.spring.io/milestone</url>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
		</pluginRepository>
	</pluginRepositories>

</project>
