package com.arkrev.grpcse.manager.impl;

import static tech.tablesaw.aggregate.AggregateFunctions.earliestDate;
import static tech.tablesaw.aggregate.AggregateFunctions.geometricMean;
import static tech.tablesaw.aggregate.AggregateFunctions.latestDate;
import static tech.tablesaw.aggregate.AggregateFunctions.max;
import static tech.tablesaw.aggregate.AggregateFunctions.mean;
import static tech.tablesaw.aggregate.AggregateFunctions.median;
import static tech.tablesaw.aggregate.AggregateFunctions.min;
import static tech.tablesaw.aggregate.AggregateFunctions.quadraticMean;
import static tech.tablesaw.aggregate.AggregateFunctions.range;
import static tech.tablesaw.aggregate.AggregateFunctions.stdDev;
import static tech.tablesaw.aggregate.AggregateFunctions.sum;
import static tech.tablesaw.aggregate.AggregateFunctions.variance;

import java.io.IOException;
import java.time.LocalDateTime;

import org.springframework.stereotype.Component;

import com.arkrev.grpcse.grpc.ExtraData;
import com.arkrev.grpcse.grpc.TimeSeries;
import com.arkrev.grpcse.grpc.TimeSeries.Builder;
import com.arkrev.grpcse.grpc.TimeSeriesData;
import com.arkrev.grpcse.manager.EnergyManager;
import com.google.protobuf.ByteString;

import lombok.extern.slf4j.Slf4j;
import tech.tablesaw.api.Row;
import tech.tablesaw.api.Table;
import tech.tablesaw.plotly.api.TimeSeriesPlot;
import tech.tablesaw.plotly.components.Figure;

@Slf4j
@Component
public class EnergyManagerImpl implements EnergyManager {

	@Override
	public TimeSeries getEnergyConsumption(LocalDateTime time) throws IOException {
		Table meterusageTable = Table.read().csv("src/main/resources/data/meterusage.csv");

		// We keep all result that match the time
		Table filteredTable = meterusageTable.where(meterusageTable.dateTimeColumn("time").isEqualTo(time));

		// We return the first result found
		//@formatter:off
		return TimeSeries.newBuilder()
				.addTimeSeriesData(TimeSeriesData.newBuilder()
						.setTime(filteredTable.getString(0, 0))
						.setValue(filteredTable.getString(0, 1))
						.build())
				.build();
		//@formatter:on
	}

	@Override
	public TimeSeries getEnergyConsumption() throws IOException {
		// init return
		Builder timeSeriesBuilder = TimeSeries.newBuilder();

		// parse csv
		Table meterusageTable = Table.read().csv("src/main/resources/data/meterusage.csv");
		for (Row row : meterusageTable) {
			//@formatter:off
			timeSeriesBuilder
					.addTimeSeriesData(TimeSeriesData.newBuilder()
							.setTime(row.getDateTime(0).toString())
							.setValue(String.valueOf(row.getDouble(1)))
							.build());
			//@formatter:on
		}

		// add some extra information
		timeSeriesBuilder.setExtraData(this.getExtraData(meterusageTable));

		return timeSeriesBuilder.build();

	}

	private ExtraData getExtraData(Table table) {
		Figure timeSeriesPlot = TimeSeriesPlot.create("ElectricityConsumption", table, "time", "meterusage");
//		table.summarize("meterusage", mean, max, min).apply();

		return ExtraData.newBuilder()
				.setGraph(ByteString.copyFrom(timeSeriesPlot.asJavascript("meterusage").getBytes()))
				.setStats(table.summarize("meterusage", earliestDate, latestDate, sum, median, mean, geometricMean,
						quadraticMean, max, min, range, variance, stdDev).apply().print())
				.build();
	}

}
