package com.arkrev.grpcse.manager;

import java.io.IOException;
import java.time.LocalDateTime;

import com.arkrev.grpcse.grpc.TimeSeries;

public interface EnergyManager {

	TimeSeries getEnergyConsumption(LocalDateTime time) throws IOException;

	TimeSeries getEnergyConsumption() throws IOException;

}
