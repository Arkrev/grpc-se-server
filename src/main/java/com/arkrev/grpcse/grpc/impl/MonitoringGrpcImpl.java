package com.arkrev.grpcse.grpc.impl;

import java.io.IOException;
import java.time.format.DateTimeParseException;

import javax.validation.ConstraintViolationException;

import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

import com.arkrev.grpcse.grpc.ElectricityConsumptionRequest;
import com.arkrev.grpcse.grpc.ElectricityConsumptionResponse;
import com.arkrev.grpcse.grpc.EmptyRequest;
import com.arkrev.grpcse.grpc.MonitoringGrpc.MonitoringImplBase;
import com.arkrev.grpcse.service.EnergyService;

import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@GRpcService
public class MonitoringGrpcImpl extends MonitoringImplBase {

	@Autowired
	private EnergyService energyService;

	@Override
	public void getElectricityConsumptionAtTime(ElectricityConsumptionRequest request,
			StreamObserver<ElectricityConsumptionResponse> responseObserver) {

		// Response initialization
		ElectricityConsumptionResponse.Builder response = ElectricityConsumptionResponse.newBuilder();

		try {
			// Parse arguments and compute response
			response.setTimeSeries(energyService.getEnergyConsumption(request.getTime()));
		} catch (DateTimeParseException | ConstraintViolationException e) {
			log.error("ArgumentError : getElectricityConsumption", e);
			responseObserver.onError(Status.INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
		} catch (IOException e) {
			log.error("IoError : getElectricityConsumption", e);
			responseObserver.onError(Status.UNKNOWN.withDescription(e.getMessage()).asRuntimeException());
		} catch (Exception e) {
			log.error("UnknowError : getElectricityConsumption", e);
			responseObserver.onError(Status.UNKNOWN.withDescription(e.getMessage()).asRuntimeException());
		} finally {
			responseObserver.onNext(response.build());
			responseObserver.onCompleted();
		}

	}

	@Override
	public void getElectricityConsumption(EmptyRequest request,
			StreamObserver<ElectricityConsumptionResponse> responseObserver) {

		// Response initialization
		ElectricityConsumptionResponse.Builder response = ElectricityConsumptionResponse.newBuilder();

		try {
			// Parse arguments and compute response
			response.setTimeSeries(energyService.getEnergyConsumption());
		} catch (DateTimeParseException | ConstraintViolationException e) {
			log.error("ArgumentError : getElectricityConsumption", e);
			responseObserver.onError(Status.INVALID_ARGUMENT.withDescription(e.getMessage()).asRuntimeException());
		} catch (IOException e) {
			log.error("IoError : getElectricityConsumption", e);
			responseObserver.onError(Status.UNKNOWN.withDescription(e.getMessage()).asRuntimeException());
		} catch (Exception e) {
			log.error("UnknowError : getElectricityConsumption", e);
			responseObserver.onError(Status.UNKNOWN.withDescription(e.getMessage()).asRuntimeException());
		} finally {
			responseObserver.onNext(response.build());
			responseObserver.onCompleted();
		}

	}

}
