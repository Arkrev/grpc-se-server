package com.arkrev.grpcse.service.impl;

import java.io.IOException;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.arkrev.grpcse.grpc.TimeSeries;
import com.arkrev.grpcse.manager.EnergyManager;
import com.arkrev.grpcse.service.EnergyService;

@Service
public class EnergyServiceImpl implements EnergyService {

	@Autowired
	private EnergyManager energyManager;

	@Override
	public TimeSeries getEnergyConsumption(String time) throws IOException {
		return energyManager.getEnergyConsumption(LocalDateTime.parse(time));
	}

	@Override
	public TimeSeries getEnergyConsumption() throws IOException {
		return energyManager.getEnergyConsumption();
	}

}
