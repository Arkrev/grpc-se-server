package com.arkrev.grpcse.service;

import java.io.IOException;

import javax.validation.constraints.NotBlank;

import org.springframework.validation.annotation.Validated;

import com.arkrev.grpcse.grpc.TimeSeries;

@Validated
public interface EnergyService {

	public TimeSeries getEnergyConsumption(@NotBlank String time) throws IOException;

	public TimeSeries getEnergyConsumption() throws IOException;

}
