FROM maven:3.5.2-jdk-8 AS MAVEN_TOOL_CHAIN
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package

FROM openjdk:9
COPY --from=MAVEN_TOOL_CHAIN /usr/src/app/target/*.jar /usr/app/grpc-se-server.jar 
EXPOSE 8081
ENTRYPOINT ["java","-jar","/usr/app/grpc-se-server.jar"]